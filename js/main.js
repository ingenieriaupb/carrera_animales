var carrera,bonus_btn;
var avance_animal = 100;
var ruta_imagenes = "img/";
var total_jinetes = 0;
var error_01 = "No es posible agregar más jinetes";
var error_02 = "No hay un jinete asignado";
var ref_jinetes = [];
var imgs = ["jirafa_mini.png","leon_mini.png","leopardo_mini.png","mapache_mini.png","pug_mini.png","zorro_mini.png","aguila_mini.png","ardilla_mini.png","ave_mini.png","husky_mini.png"];
var fondos = ["animado_01.gif","animado_02.gif","animado_03.gif","animado_04.gif","animado_05.gif","animado_06.gif","animado_07.gif","animado_08.gif"];
var fondo_gif, contador_videos = 0, id_rotador_videos;
var socket_keep_alive;
var socket;
var cont = 0;

window.onload = init;

function init()
{
	crearVariables();
	crearImagenes();
	crearSocket();
	mantenerPistaViva();
	rotarVideosPrehome();
}

function crearLogicaPrehome()
{
	fondo_gif.style.display = 'none';
	carrera.style.display = 'block';
	clearInterval(id_rotador_videos);
}

function rotarVideosPrehome(){
	id_rotador_videos = setInterval(function(){
		contador_videos = (contador_videos+1<fondos.length)?contador_videos+1:0;
		fondo_gif.style.backgroundImage = "url("+ruta_imagenes+fondos[contador_videos]+")";
		fondo_gif.style.color = contador_videos%2==0?"#eddd00":"#515151";
	},4000);
}

function mantenerPistaViva(){
	socket_keep_alive = setInterval(function(){
		try{
			socket.emit('pista_viva',{});
		}
		catch(e)
		{
			clearInterval(socket_keep_alive);
			console.log("Error de conexión de socket");
		}
	},5000);
	var el = document.getElementById("closeBtn");
	el.addEventListener("click", cerrarVentana, false); 
}

function cerrarVentana(){
	var ventana = document.getElementById("popupVictoria");
	ventana.style.display = "none";
	window.location.reload(true);
}
function openVentana(){
	var ventana = document.getElementById("popupVictoria");
	ventana.style.display = "block";
}

function crearSocket()
{
	try
	{
		socket = io();
		socket.emit('asignar_pista',"Nombre pista");
		socket.on('asignarJinete', function(socket_id){
			if(cont < 10){
				asignarJinete(socket_id);
				var path = document.getElementById(socket_id).children[1].src;
				socket.emit('asignar_imagen',{socket_id:socket_id,path:path});
				cont++;
				document.getElementById("usuarios_conectados").innerHTML = cont;
			}

			if(cont >= 10){
				socket.emit('empezar_partida',{});
				crearLogicaPrehome();
			}

	  	});
	  	socket.on('removerJinete', function(socket_id){
	    	removerJinete(socket_id);
	  	});
	  	socket.on('moverAnimal', function(data){  		
	    	moverAnimal(data);
	  	});
	}
	catch(e){
			console.log("Error de conexión de socket");
	}
}
function activarBonus()
{
	console.log("bonus");
	socket.emit('bonus',{});
}
function crearVariables()
{
	fondo_gif = document.getElementById('prehome'); 
	carrera = document.getElementById('carrera');
	bonus_btn = document.getElementById('bonus_btn');
}
function crearImagenes()
{
	var html = "";
	shuffle(imgs);
	for (var i = 0; i < 10; i++) {
		html +="<div class='track'><div class='plantas'></div><img src='"+ruta_imagenes+imgs[i]+"' class='animal img-responsive' /></div>";
	}
	carrera.innerHTML += html;
}

function asignarJinete(id)
{
	var jinete, animal;
	try{

		if(total_jinetes<10)
		{
			jinete = retornarAnimal();
			animal = retornarAnimalImg();
			jinete.id = id;
			animal.id = "img_"+id;
			ref_jinetes[id] = jinete;
			total_jinetes++;
		}
		else
		{
			console.log(error_01);
		}
	}
	catch(e)
	{
		console.error(e);
	}
}

function retornarAnimal()
{
	var animales = document.getElementsByClassName("track");

	for (var i = 0; i < animales.length; i++) {
		if(!animales[i].id)
		{
			return animales[i];
		}
	};	
}
function retornarAnimalImg(){
	var animales = document.getElementsByClassName("animal");

	for (var i = 0; i < animales.length; i++) {
		if(!animales[i].id)
		{
			return animales[i];
		}
	};
}

function removerJinete(id)
{
	if(!ref_jinetes[id])return;
	ref_jinetes[id].id = "";
	ref_jinetes[id] = undefined;
	total_jinetes--;
}

function moverAnimal(data)
{
	var jinete, pos_x;
	var id = "/#"+data.socket_id;
	avance_animal = data.velocidad;
	
	if(!id || id === "/#")
	{
		console.log(error_02);
		return;	
	} 
	console.log(id);
	jinete = ref_jinetes[id].children[1];
	pos_x = !jinete.style.marginLeft?1:jinete.style.marginLeft;
	jinete.style.marginLeft = (parseInt(pos_x)+avance_animal)+"px";

	var element = document.getElementById("img_"+id);

	var element2 = document.getElementById("carrera");
	var rect2 = element2.getBoundingClientRect();

	if(rect2.right-element.offsetWidth > 0){
		if(!((rect2.right-element.offsetWidth) > ((parseInt(pos_x)+avance_animal)+element.offsetWidth))){
			openVentana();
		}
	}


}

function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}