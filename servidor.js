var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var pista_id;
var jugadores = [];

function asignarParticipante(socket_id)
{
	enviarMensajeAPista("asignarJinete",socket_id);
}

function enviarMensajeAPista(event,data)
{
	if(!pista_id)return;
	io.to(pista_id).emit(event,data);
}

io.on('connection', function(socket){
	
	asignarParticipante(socket.id);
	socket.on('disconnect', function(){
		if(socket.id==pista_id)
		{
			console.log('pista desconectada '+socket.id);
		}
		else
		{
			enviarMensajeAPista("removerJinete",socket.id);
			console.log('jinete desconectado '+socket.id);
		}
	});	
	socket.on("pista_viva",function(data){
		console.log("pista viva");
	});
	socket.on('asignar_imagen',function(data){
		io.to(data.socket_id).emit("asignarImagen",data.path);
	});
	socket.on('empezar_partida',function(data){
		io.sockets.emit("empezar_partida",data);
	});
	socket.on('bonus',function(data){
		console.log("bonus!!!");
		io.sockets.emit("bonus",{});
	});
	socket.on('mover_animal', function(data){
		enviarMensajeAPista("moverAnimal",data);
	});
	socket.on('asignar_pista',function(data){
		pista_id = socket.id;
	});
});

app.get('/', function(req, res){
	res.sendFile(__dirname+'/index.html');
});

app.get("/jugador",function(reg,res){
	res.sendFile(__dirname+"/jugador.html");
});

app.use("/img",express.static(__dirname+"/img"));
app.use("/js",express.static(__dirname+"/js"));
app.use("/css",express.static(__dirname+"/css"));

http.listen(3000, function(){
	console.log('listening on *:3000');
});